<?php

namespace App\Providers;

use App\Composers\CategoryComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('client.layouts.app', CategoryComposer::class);
    }
}
