<?php

namespace App\Models;

use App\Traits\HandleImageTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Stmt\Return_;

class Product extends Model
{
    // , HandleImageTrait;
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'sale',
        'price'
    ];

    public function details()
    {
        return $this->hasMany(ProductDetail::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    // public function getBy($dateSearch , $categoryId)
    // {
    //     return $this->whereHas('categories', fn($q) =>$q->where('id', $categoryId) ->paginate(10));
    // }
}
