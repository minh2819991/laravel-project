<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permissions extends Permissions
{
    use HasFactory;

    protected $fillable = [
        'name',
        'display_name',
        'group'
    ];
}
