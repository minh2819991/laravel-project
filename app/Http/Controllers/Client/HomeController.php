<?php

namespace App\Http\Controllers\Client;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        $categories = Category::get(['id','name']);
        $products = $this->product->latest('id')->paginate(10);
        return view('client.home.index', compact('products' , 'categories'));
    }
}
