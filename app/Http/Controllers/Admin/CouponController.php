<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Coupons\CreateConponRequest;
use App\Http\Requests\Coupons\UpdateConponRequest;
use App\Models\Coupon;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $coupon;
    public function __construct(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }
    public function index()
    {
        $coupons = $this->coupon->latest('id')->paginate(3);
        return view('admin.coupons.index' , compact('coupons')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateConponRequest $request)
    {
        $dataCreate = $request->all();
        $this->coupon->create($dataCreate);

        return redirect()->route('coupons.index')->with(['message' => ' Create Success']);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupons = $this->coupon->findOrFail($id);
        return view('admin.coupons.edit',compact('coupons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateConponRequest $request, $id)
    {
        $coupon = $this->coupon->findOrFail($id);
        $dateUpdate = $request->all();
        $coupon->update($dateUpdate);
        return redirect()->route('coupons.index')->with(['message' => ' Create Success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Coupon::destroy($id);
        return to_route('coupons.index')->with(['message' => 'Delete success']);
    }
}
