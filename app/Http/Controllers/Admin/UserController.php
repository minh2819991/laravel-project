<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\User\CreaterUserRequest;
use App\Http\Requests\User\UpdateUserRequest as UserUpdateUserRequest;
use App\Http\Requests\Users\CreateUserRequest as UsersCreateUserRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    protected $user;
    protected $role;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(User $user , Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }
    public function index()
    {
        $users = $this->user->latest('id')->paginate(5);
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->role->all()->groupBy('group');
        return view('admin.users.create' , compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreaterUserRequest $request)
    {
        $dateCreate = $request->all();
        $dateCreate['password'] =Hash::make($request->password);
        // $dateImage['image'] = $this->user->saveImage($request);

        $user = $this->user->create($dateCreate);
        $user->roles()->attach($dateCreate['role_ids']);
        return to_route('users.index')->with(['message' => 'Create success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user->findOrFail($id)->load('roles');
        $roles = $this->role->all()->groupBy('group');
        return view('admin.users.edit', compact('user' ,'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateUserRequest $request, $id)
    {
        $dateUpdate = $request->except('password');
        $user = $this->user->findOrFail($id)->load('roles');
        if($request->password)
        {
            $dateCreate['password'] = Hash::make($request->password);
        }
        // $dateImage['image'] = $this->user->saveImage($request);


        $user->update($dateUpdate);
        $user->roles()->sync($dateUpdate['role_ids']);
        return to_route('users.index')->with(['message' => 'Update success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->user->findOrFail($id)->load('roles');
        $user->delete();
        return to_route('users.index')->with(['message' => 'Delete success']);
    }
}
