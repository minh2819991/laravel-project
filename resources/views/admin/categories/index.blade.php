@extends('admin.dashboard.index')
@section('title', 'Category')
@section('content')
    <div class="card">

        <h1>Categories list</h1>
        @if(session('message'))
            <h2 class="text-primary">{{session('message') }}</h2>
        @endif
        <div>
            <a href="{{route('categories.create')}}" class="btn btn-primary">Create</a>
        </div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Parent Name</th>
                <th>Action</th>
            </tr>

            @foreach ($categories as $item)
                <tr>
                    <td>{{$item -> id}}</td>
                    <td>{{$item -> name}}</td>
                    <td>{{$item -> parent_name}}</td>
                   
                    <td style="display: flex">
                        <a href="{{route('categories.edit', $item->id )}}" class="btn btn-warning">Edit</a>
                        <form action="{{route('categories.destroy', $item->id )}} " id="form-delete{{$item->id}}"
                            method="POST"> 
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger" type="submit"  data-id="{{$item->id}}">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{$categories->links()}}
    </div>
@endsection