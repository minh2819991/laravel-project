@extends('admin.dashboard.index')
@section('title', 'Create Coupon')
@section('content')
    <div class="cart">
        <h1>Create Coupon</h1>
            <form action="{{route('coupons.store')}}" method="post" >
                @csrf 
                <div class="input-group input-group-static mb-4">
                    <label>Name</label>
                    <input type="text" value="{{old('name')}}" name="name" class="form-control" style="text-transform: uppercase">
    
                    @error('name')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="input-group input-group-static mb-4">
                    <label>Value</label>
                    <input type="number" value="{{old('value')}}" name="value" class="form-control">
    
                    @error('value')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="input-group input-group-static mb-4">
                    <label  class="ms-0">Type</label>
                    <select name="type" class="form-control" value="{{old('type')}}">
                       <option >Select Type</option>
                       <option >Money</option>
                    </select>
    
                    @error('type')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Expery Day</label>
                    <input type="date" value="{{old('expery_date')}}" name="expery_date" class="form-control">
    
                    @error('expery_date')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>
            </form>
    </div>
@endsection