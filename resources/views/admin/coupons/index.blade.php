@extends('admin.dashboard.index')
@section('title', 'Coupon')
@section('content')
    <div class="card">

        <h1>Coupon list</h1>
        @if(session('message'))
            <h2 class="text-primary">{{session('message') }}</h2>
        @endif
        <div>
            <a href="{{route('coupons.create')}}" class="btn btn-primary">Create</a>
        </div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Type</th>
                <th>Value</th>
                <th>Expery Date</th>
                <th>Action</th>
            </tr>

            @foreach ($coupons as $item)
                <tr>
                    <td>{{$item -> id}}</td>
                    <td>{{$item -> name}}</td>
                    <td>{{$item -> type}}</td>
                    <td>{{$item -> value}}</td>
                    <td>{{$item -> expery_date}}</td>
                    <td style="display: flex">
                        <a href="{{route('coupons.edit', $item->id )}}" class="btn btn-warning">Edit</a>
                        <form action="{{route('coupons.destroy', $item->id )}} " id="form-delete{{$item->id}}"
                            method="POST"> 
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger" type="submit"  data-id="{{$item->id}}">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{$coupons->links()}}
    </div>
@endsection