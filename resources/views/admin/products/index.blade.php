@extends('admin.dashboard.index')
@section('title', 'Product')
@section('content')
    <div class="card">

        <h1>Categories list</h1>
        @if(session('message'))
            <h2 class="text-primary">{{session('message') }}</h2>
        @endif
        <div>
            <a href="{{route('products.create')}}" class="btn btn-primary">Create</a>
        </div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Sale</th>
                <th>Price</th>
                <th>Action</th>
            </tr>

            @foreach ($products as $item)
                <tr>
                    <td>{{$item -> id}}</td>
                    <td>{{$item -> name}}</td>
                    <td>{{$item -> sale}}</td>
                    <td>{{$item -> price}}</td>
                   
                    <td style="display: flex">
                        <a href="{{route('products.edit', $item->id )}}" class="btn btn-warning">Edit</a>
                        <form action="{{route('products.destroy', $item->id )}} " id="form-delete{{$item->id}}"
                            method="POST"> 
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger" type="submit"  data-id="{{$item->id}}">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{$products->links()}}
    </div>
@endsection