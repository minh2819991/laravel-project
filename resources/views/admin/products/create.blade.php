@extends('admin.dashboard.index')
@section('title', 'Product Create')
@section('content')
    <div class="cart">
        <h1>Create Product</h1>
        
            <form action="{{route('products.store')}}" method="post">
                @csrf 
                <div class="input-group input-group-static mb-4">
                    <label>Name</label>
                    <input type="text" value="{{old('name')}}" name="name" class="form-control">
    
                    @error('name')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
    
                <div class="input-group input-group-static mb-4">
                    <label>Price</label>
                    <input type="number" value="{{old('price')}}" name="price" class="form-control">
    
                    @error('price')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
    
                
                <div class="input-group input-group-static mb-4">
                    <label>Sale</label>
                    <input type="text" value="{{ old('sale') }}" name="sale" class="form-control">
    
                    @error('sale')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Description</label>
                    <input type="text" value="{{ old('description') }}" name="description" class="form-control">
    
                    @error('descriptione')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label  class="ms-0">Category</label>
                    <select name="category_ids[]" class="form-control" multiple>
                        @foreach ($categories as $item )
                            <option value="{{$item->id}}"> {{$item->name}}</option>
                        @endforeach
                    </select>
    
                    @error('gender')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>
            </form>
    </div>
@endsection