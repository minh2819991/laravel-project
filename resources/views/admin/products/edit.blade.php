@extends('admin.dashboard.index')
@section('title', 'Product Edit')
@section('content')
    <div class="cart">
        <h1>Edit Product</h1>
        
            <form action="{{route('products.update' , $product->id)}}" method="post">
                @csrf 
                @method('PUT')
                <div class="input-group input-group-static mb-4">
                    <label>Name</label>
                    <input type="text" value="{{old('name') ?? $product->name}}" name="name" class="form-control">
    
                    @error('name')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
    
                <div class="input-group input-group-static mb-4">
                    <label>Price</label>
                    <input type="number" value="{{old('price') ?? $product->price}}" name="price" class="form-control">
    
                    @error('price')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
    
                
                <div class="input-group input-group-static mb-4">
                    <label>Sale</label>
                    <input type="text" value="{{ old('sale')?? $product->sale }}" name="sale" class="form-control">
    
                    @error('sale')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Description</label>
                    <input type="text" value="{{ old('description')?? $product->description }}" name="description" class="form-control">
    
                    @error('descriptione')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label  class="ms-0">Category</label>
                    <select name="category_ids[]" class="form-control" multiple>
                        @foreach ($categories as $item )
                            <option value="{{$item->id}}" {{$product->categories->contains('id',$item->id) ? 'selected' : ''}}> {{$item->name}}</option>
                        @endforeach
                    </select>
    
                    @error('gender')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-submit btn-primary">Submit</button>
            </form>
    </div>
@endsection