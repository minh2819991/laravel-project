@extends('admin.dashboard.index')
@section('title', 'Users')
@section('content')
    <div class="card">

        <h1>User list</h1>
        @if(session('message'))
            <h2 class="text-primary">{{ session('message') }}</h2>
        @endif
        <div>
            <a href="{{route('users.create')}}" class="btn btn-primary">Create</a>
        </div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>

            @foreach ($users as $item)
                <tr>
                    <td>{{$item -> id}}</td>
                    <td>{{$item -> name}}</td>
                    <td>{{$item -> email}}</td>
                    <td>{{$item -> phone}}</td>
                    <td style="display: flex">
                        <a href="{{route('users.edit', $item->id )}}" class="btn btn-warning">Edit</a>
                        <form action="{{route('users.destroy', $item->id )}} " id="form-delete{{$item->id}}"
                            method="POST"> 
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger" type="submit"  data-id="{{$item->id}}">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{$users->links()}}
    </div>
@endsection