@extends('admin.dashboard.index')
@section('title', 'Roles')
@section('content')
    <div class="card">
    
        <h1>Role list</h1>
        @if(session('message'))
            <h2 class="text-primary">{{ session('message') }}</h2>
        @endif
        <div>
            <a href="{{route('roles.create')}}" class="btn btn-primary">Create</a>
        </div>
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>DisplayName</th>
                <th>Action</th>
            </tr>

            @foreach ($roles as $role)
                <tr>
                    <td>{{$role -> id}}</td>
                    <td>{{$role -> name}}</td>
                    <td>{{$role -> display_name}}</td>
                    <td style="display: flex">
                        <a href="{{route('roles.edit', $role->id )}}" class="btn btn-warning">Edit</a>
                        <form action="{{route('roles.destroy', $role->id )}} " method="POST"> 
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger">Delete</button>
                        </form>

                    </td>
                </tr>
            @endforeach
        </table>
        {{$roles->links()}}
    </div>
@endsection