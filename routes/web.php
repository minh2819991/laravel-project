<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CouponController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Client\CartController;
use App\Http\Controllers\Client\HomeController;
use App\Http\Controllers\Client\ProductController as ClientProductController;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'index'])->name('client.home');


Route::get('product', [ClientProductController::class,'index'])->name('client.products.index');
Route::get('product-detail/{id}', [ClientProductController::class,'show'])->name('client.products.show');

Auth::routes();

Route::middleware('auth')->group(function(){
    Route::post('add-to-cart', [CartController::class,'store'])->name('client.product.show');

});
Route::middleware('auth')->group(function(){
    Route::get('/dashboard', function () {
        return view('admin.dashboard.index');
    })->name('dashboard'); 
    
Route::resource('roles', RoleController::class  );
Route::resource('users', UserController::class  );
Route::resource('categories' , CategoryController::class, ['index']);
Route::resource('products', ProductController::class);
Route::resource('coupons', CouponController::class);

});


